package rssreader;

import java.net.*;
import java.io.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;

/**
 *
 * @author Alexander Korzhikov <korzio@gmail.com>
 */
public class URLConnectionReader {
    private DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();        
    
    String url;
    Document source;    
    
//    URLConnectionReader( String url ) {
//        this.url = url;
//        
//        try {
//            URL oracle = new URL( url );
//            URLConnection yc = oracle.openConnection();
//            BufferedReader in = new BufferedReader(new InputStreamReader(
//                                        yc.getInputStream()));
//            String inputLine;
//            String result = "";
//            while ((inputLine = in.readLine()) != null) 
//                result += inputLine;
//            
//            in.close();
//            
//            this.source = result;
//        }
//        catch( Exception e ) {
//            System.out.println( "Error on reading url :: " + url );
//        }
//    }
    
    URLConnectionReader( String url ) {
        this.url = url;
               
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            this.source = db.parse( url );
        }
        catch( Exception e ) {
            System.out.println( "Error on reading url :: " + url );
        }
    }
    
    Document getSource(){
        return this.source;
    }
}
