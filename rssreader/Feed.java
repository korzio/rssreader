package rssreader;

import java.util.*;
import org.w3c.dom.*;

/**
 *
 * @author Alexander Korzhikov <korzio@gmail.com>
 */
public class Feed extends ArrayList {
    String source;
    
    Feed( Document doc ) {
        NodeList elements = doc.getElementsByTagName( "enclosure" );
        if( elements.getLength() == 0 ) 
            return;
        for (int i = 0; i < elements.getLength(); i++) {
            FeedMessage msg = new FeedMessage( elements.item(i) );            
            this.add( msg );
        }
    }
    
    Feed(ArrayList<String> list){       
        
        for(int i = 0; i < list.size(); i++){
            FeedMessage msg = new FeedMessage( list.get(i) );
            this.add( msg );
        }
    }
}
