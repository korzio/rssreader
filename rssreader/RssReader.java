package rssreader;

import java.util.*;

/**
 *
 * @author Alexander Korzhikov <korzio@gmail.com>
 */


public class RssReader {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        System.out.println("start");        
        URLConnectionReader connection = new URLConnectionReader( "http://echo.msk.ru/programs/penie/rss-audio.xml" );
        //URLConnectionReader connection = new URLConnectionReader( "http://echo.msk.ru/programs/opera/rss-audio.xml" );
        Feed feed = new Feed( connection.getSource() );
        //Feed feed = new Feed(FeedOperniyClub.list);
        
        FileDownloader download = new FileDownloader( feed );
    }
}
