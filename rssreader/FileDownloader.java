package rssreader;

import java.util.*;
import java.io.*;
import java.net.*;

/**
 *
 * @author Alexander Korzhikov <korzio@gmail.com>
 */
public class FileDownloader{
    String dir;
    
    FileDownloader( Feed feed ) {
        
        ListIterator<FeedMessage> it = feed.listIterator();
        while( it.hasNext() ) {
            FeedMessage msg = (FeedMessage)it.next();
            
            if( !msg.getUrl().isEmpty() && !msg.getTitle().isEmpty() )
                try {
                    /*
                     * Get a connection to the URL and start up
                     * a buffered reader.
                     */
                    long startTime = System.currentTimeMillis();
                    System.out.println("Starting file ");

                    URL url = new URL( msg.getUrl() );
                    url.openConnection();
                    InputStream reader = url.openStream();

                    /*
                     * Setup a buffered file writer to write
                     * out what we read from the website.
                     */
                    FileOutputStream writer = new FileOutputStream("/Users/alex/Works/Java/RssReader/test/" + msg.getTitle() );
                    byte[] buffer = new byte[153600];
                    int totalBytesRead = 0;
                    int bytesRead = 0;

                    System.out.println("Reading ZIP file 150KB blocks at a time.\n");

                    while ((bytesRead = reader.read(buffer)) > 0)
                    {  
                       writer.write(buffer, 0, bytesRead);
                       buffer = new byte[153600];
                       totalBytesRead += bytesRead;
                    }

                    long endTime = System.currentTimeMillis();

                    System.out.println("Done. " + (new Integer(totalBytesRead).toString()) + " bytes read (" + (new Long(endTime - startTime).toString()) + " millseconds).\n");
                    writer.close();
                    reader.close();
                 }
                 catch (MalformedURLException e)
                 {
                    e.printStackTrace();
                 }
                 catch (IOException e)
                 {
                    e.printStackTrace();
                 }
        }
    }
}

