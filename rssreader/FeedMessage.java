package rssreader;

import java.util.HashSet;
import java.util.Set;
import org.w3c.dom.*;

/**
 *
 * @author Alexander Korzhikov <korzio@gmail.com>
 */
public class FeedMessage {    
    String title;
    String url;
    String source;
    
    FeedMessage( String originalSource ) {
        this.setSource( originalSource );
        this.setUrl( originalSource );
        
        int index = this.url.lastIndexOf( '/' );
        if( index != -1 )
            this.setTitle( this.url.substring( index ) );
    }
    
    FeedMessage( Node node ) {
        if( node.hasAttributes() ) {
            NamedNodeMap map = node.getAttributes();
            this.setUrl( map.getNamedItem("url").getNodeValue() );
            
            int index = this.url.lastIndexOf( '/' );
            if( index != -1 )
                this.setTitle( this.url.substring( index ) );
        }
    }    
    
    void setSource( String source ) {
        this.source = source;
    }
    void setTitle( String title ) {
        this.title = title;
    }
    String getTitle() {
        return this.title;
    }
    void setUrl( String url ) {
        this.url = url;
    }
    String getUrl() {
        return this.url;
    }
}
